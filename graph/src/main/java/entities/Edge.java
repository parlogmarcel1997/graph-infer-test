package entities;

import java.util.Objects;

public class Edge {
    private int id;
    private Vertex source;
    private Vertex destination;

    public Edge(int id, Vertex source, Vertex destination) {
        this.id = id;
        this.source = source;
        this.destination = destination;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vertex getSource() {
        return source;
    }

    public void setSource(Vertex source) {
        this.source = source;
    }

    public Vertex getDestination() {
        return destination;
    }

    public void setDestination(Vertex destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;
        Edge edge = (Edge) o;
        return getId() == edge.getId() &&
                getSource().equals(edge.getSource()) &&
                getDestination().equals(edge.getDestination());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSource(), getDestination());
    }

    @Override
    public String toString() {
        return "Edge{" +
                "id=" + id +
                ", source=" + source +
                ", destination=" + destination +
                '}';
    }
}
