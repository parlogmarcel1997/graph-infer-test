package entities;

import java.util.List;
import java.util.Objects;

public class Vertex {
    private int id;
    private List<Integer> destinations;
    public Vertex(int id, List<Integer> destinations) {
        this.id = id;
        this.destinations = destinations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Integer> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Integer> destinations) {
        this.destinations = destinations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vertex)) return false;
        Vertex vertex = (Vertex) o;
        return getId() == vertex.getId() &&
                getDestinations().equals(vertex.getDestinations());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDestinations());
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "id=" + id +
                ", destinations=" + destinations +
                '}';
    }
}
