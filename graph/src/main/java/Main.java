import algo.Difference;
import entities.Edge;
import entities.Graph;
import entities.Vertex;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Main {

    private static Graph buildGraph(int numberOfVertex){
        Graph graph = new Graph();
        Random random = new Random();
        List<Integer> integers = getIds();

        for (int i = 0; i < numberOfVertex; i++){
            Integer integer = integers.get(random.nextInt(integers.size()));
            integers.remove(integer);
            Vertex vertex = new Vertex(integer, new LinkedList<>());
            graph.getVertices().add(vertex);
        }

        for(int i = 0; i < (int)(numberOfVertex + 0.5 * numberOfVertex); i++){
            List<Vertex> vertices = new LinkedList<>(graph.getVertices());
            Vertex vertex1 = vertices.get(random.nextInt(vertices.size()));
            vertices.remove(vertex1);
            Vertex vertex2 = vertices.get(random.nextInt(vertices.size()));

            if(Math.random() > 0.5){
                Edge edge = new Edge(i, vertex1, vertex2);
                Objects.requireNonNull(graph.getVertices().stream()
                        .filter(v -> v.getId() == vertex1.getId())
                        .findFirst()
                        .orElse(null))
                        .getDestinations()
                        .add(vertex2.getId());
                graph.getEdges().add(edge);
            } else {
                Edge edge = new Edge(i, vertex2, vertex1);
                Objects.requireNonNull(graph.getVertices().stream()
                        .filter(v -> v.getId() == vertex2.getId())
                        .findFirst()
                        .orElse(null))
                        .getDestinations()
                        .add(vertex1.getId());
                graph.getEdges().add(edge);
            }
        }
        return graph;
    }

    private static List<Integer> getIds(){
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 20; i++){
            list.add(i);
        }
        return list;
    }

    public static void main(String[] args) {
        Graph graph1 = buildGraph(10);
        Graph graph2 = buildGraph(7);
        Graph result = Difference.graphDifference(graph1, graph2);
        System.out.println(result);
    }
}
