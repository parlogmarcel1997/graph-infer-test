package algo;

import entities.Edge;
import entities.Graph;
import entities.Vertex;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class Difference {
    public static Graph graphDifference(Graph graph1, Graph graph2){
        Graph result = new Graph();
        result.setVertices(graph1.getVertices());
        result.setEdges(graph1.getEdges());

        List<Vertex> vertices = new CopyOnWriteArrayList<>(result.getVertices());
        for(Vertex v : vertices){
            if(graph2.existVertexId(v.getId())){
                for(Integer i : v.getDestinations()){
                    Edge edge = result.getEdges().stream()
                            .filter(e -> e.getSource().getId() == v.getId() && e.getDestination().getId() == i)
                            .findFirst()
                            .get();
                    result.getEdges().remove(edge);
                }
                List<Edge> edgesForDelete = result.getEdges()
                        .stream()
                        .filter(e -> e.getDestination().getId() == v.getId())
                        .collect(Collectors.toList());
                for(Edge e : edgesForDelete){
                    vertices.stream()
                            .filter(v1 -> v1.getId() == e.getDestination().getId())
                            .findFirst()
                            .get()
                            .getDestinations()
                            .remove(Integer.valueOf(e.getDestination().getId()));
                }
                result.getEdges().removeAll(edgesForDelete);
                vertices.remove(v);
            }
        }
        result.setVertices(new LinkedList<>(vertices));
        return result;
    }
}
